#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@s160902
Feature: Payout features.

  @s160902
  Scenario: I can create and delete an account.
  	Given Account "123456-7890" does not exist.
    When Create account firstname: "Bob" lastname: "The Builder" cpr-number: "123456-7890" initial-balance: 100
    Then The account with cpr-number "123456-7890" is deleted
    
  @s160902
  Scenario: I can check the balance of an account.
  	Given Account "123456-7890" does not exist.
    When Create account firstname: "Bob" lastname: "The Builder" cpr-number: "123456-7890" initial-balance: 100
    Then The account identified by "123456-7890" now has a balance of 100 
    And The account with cpr-number "123456-7890" is deleted
     
  @s160902
	Scenario: I can make a transfer between two accounts.
		Given Account "123456-7890" does not exist.
		Given Account "098765-4321" does not exist.
		When Create account firstname: "Bob" lastname: "The Builder" cpr-number: "123456-7890" initial-balance: 100
		And Create account firstname: "Barbie" lastname: "Girl" cpr-number: "098765-4321" initial-balance: 100
		And Transfer from "098765-4321" to "123456-7890" of 100 reason being "Mowing the lawn"
		Then The account identified by "098765-4321" now has a balance of 0
		And The account identified by "123456-7890" now has a balance of 200
		And The account with cpr-number "123456-7890" is deleted
		And The account with cpr-number "098765-4321" is deleted
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			