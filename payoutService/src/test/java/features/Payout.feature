@s160902
Feature: Payout features, note that RabbitMQ is tested elsewhere and thus not here.

  @s160902
  Scenario: I can create and delete an account.
  	Given Account "123456-7890" does not exist.
    When Create account firstname: "Bob" lastname: "The Builder" cpr-number: "123456-7890" initial-balance: 100
    Then The account with cpr-number "123456-7890" is deleted
    
  @s160902
  Scenario: I can check the balance of an account.
  	Given Account "123456-7890" does not exist.
    When Create account firstname: "Bob" lastname: "The Builder" cpr-number: "123456-7890" initial-balance: 100
    Then The account identified by "123456-7890" now has a balance of 100 
    And The account with cpr-number "123456-7890" is deleted
     
  @s160902
	Scenario: I can make a transfer between two accounts.
		Given Account "123456-7890" does not exist.
		Given Account "098765-4321" does not exist.
		When Create account firstname: "Bob" lastname: "The Builder" cpr-number: "123456-7890" initial-balance: 100
		And Create account firstname: "Barbie" lastname: "Girl" cpr-number: "098765-4321" initial-balance: 100
		And Transfer from "098765-4321" to "123456-7890" of 100 reason being "Mowing the lawn"
		Then The account identified by "098765-4321" now has a balance of 0
		And The account identified by "123456-7890" now has a balance of 200
		And The account with cpr-number "123456-7890" is deleted
		And The account with cpr-number "098765-4321" is deleted
	
	@s160902
	Scenario: A token response can be internally transferred from the consumer to a waiting thread.
		When Consumer receives a message from token with cpr "AWESOMECPR"
		Then The message can be picked up and is "AWESOMECPR"
			
	@s160902
	Scenario: A management response can be internally transferred from the consumer to a waiting thread.
		When Consumer receives a message from management with response true
		Then The message can be picked up and is true
		
	@s160902
	Scenario: A management response can be internally transferred from the consumer to a waiting thread.
		When Consumer receives a message from management with response false
		Then The message can be picked up and is false
		
	@s160902
	Scenario: A management refund response can be internally transferred from the consumer to a waiting thread.
		When Consumer receives a refund message from management
		Then The message can be picked up and contains true
		
		
		
		
		
		
				