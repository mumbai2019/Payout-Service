package dtu.mumbai.payout;

import java.math.BigDecimal;
import org.json.JSONObject;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.mumbai.app.PayoutImplementation;
import dtu.mumbai.payout.BankCommunicationImplementation;
import dtu.mumbai.rabbitmq.Receiver;
import dtu.mumbai.rabbitmq.Transmitter;
import enums.EventEnums;

import static org.junit.Assert.*;

public class StepDef {
	BankCommunicationImplementation pay = new BankCommunicationImplementation();
	PayoutImplementation payout = new PayoutImplementation();
	ConsumerImplementation con = new ConsumerImplementation(payout);
	Receiver receive = new Receiver(con);
	Transmitter transmit = new Transmitter();
	
	
	@Given("^Account \"([^\"]*)\" does not exist\\.$")
	public void accountDoesNotExist(String arg1) throws Throwable {
	    pay.testCleanup(arg1);
	}
	@When("^Create account firstname: \"([^\"]*)\" lastname: \"([^\"]*)\" cpr-number: \"([^\"]*)\" initial-balance: (\\d+)$")
	public void createAccountFirstnameLastnameCprNumberInitialBalance(String arg1, String arg2, String arg3, int arg4) throws Throwable
	{
		pay.createAccount(arg1, arg2, arg3, new BigDecimal(arg4));
	}
	@Then("^The account with cpr-number \"([^\"]*)\" is deleted$")
	public void theAccountWithCprNumberIsDeleted(String arg1) throws Throwable
	{
	    pay.deleteAccount(arg1);
	}

	@Then("^The account identified by \"([^\"]*)\" now has a balance of (\\d+)$")
	public void theAccountIdentifiedByNowHasABalanceOf(String arg1, int arg2) throws Throwable
	{
		assertEquals(
				pay.getAccountByCprNumber(arg1).getBalance(),
				new BigDecimal(arg2));
		
	}
	@When("^Transfer from \"([^\"]*)\" to \"([^\"]*)\" of (\\d+) reason being \"([^\"]*)\"$")
	public void transferFromToOfReasonBeing(String arg1, String arg2, int arg3, String arg4) throws Throwable
	{
	    pay.transaction(arg1, arg2, new BigDecimal(arg3), arg4);
	}
	@When("^Consumer receives a message from token with cpr \"([^\"]*)\"$")
	public void consumerReceivesAMessageFromTokenWithCpr(String arg1) throws Throwable {
		
		JSONObject json = new JSONObject();
		json.put("event", EventEnums.TOKENAUTHCPREVENT);
		json.put("cpr", arg1);
		json.put("result", true);
		
		con.consumeMessage(json.toString());
	}
	@Then("^The message can be picked up and is \"([^\"]*)\"$")
	public void theMessageCanBePickedUpAndIs(String arg1) throws Throwable {
		assertEquals(payout.getTokenResponse().get(),arg1);
	}
	@When("^Consumer receives a message from management with response true$")
	public void consumerReceivesAMessageFromManagementWithResponseTrue() throws Throwable {
		JSONObject json = new JSONObject();
		json.put("event", EventEnums.USERAUTHEVENT);
		json.put("response", true);
		
		con.consumeMessage(json.toString());
	}
	@Then("^The message can be picked up and is true$")
	public void theMessageCanBePickedUpAndIsTrue() throws Throwable {
	    assertEquals(payout.getUserResponse(),true);
	}
	@When("^Consumer receives a message from management with response false$")
	public void consumerReceivesAMessageFromManagementWithResponseFalse() throws Throwable {
		JSONObject json = new JSONObject();
		json.put("event", EventEnums.USERAUTHEVENT);
		json.put("response", false);
		
		con.consumeMessage(json.toString());
	}
	@Then("^The message can be picked up and is false$")
	public void theMessageCanBePickedUpAndIsFalse() throws Throwable {
		assertEquals(payout.getUserResponse(),false);
	}
	@When("^Consumer receives a refund message from management$")
	public void consumerReceivesARefundMessageFromManagement() throws Throwable {
		JSONObject json = new JSONObject();
		json.put("event", EventEnums.REFUNDAUTHEVENT);
		json.put("response", true);
		
		con.consumeMessage(json.toString());
	}
	@Then("^The message can be picked up and contains true$")
	public void theMessageCanBePickedUpAndContainsTrue() throws Throwable {
		assertEquals(payout.getRefundResponse().get("response"),true);
	}
}
