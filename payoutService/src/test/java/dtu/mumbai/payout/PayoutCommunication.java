package dtu.mumbai.payout;

import java.math.BigDecimal;
import java.util.List;

import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.AccountInfo;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;

/**
 * For manual testing purposes.
 * @author s160902
 */
public class PayoutCommunication {
	public static void main(String[] args) throws BankServiceException_Exception
	{
		BankServiceService bank = new BankServiceService();
		BankCommunicationImplementation pay = new BankCommunicationImplementation();
		String id;
		String id2;
		
		User user = new User();
		user.setFirstName("Bob");
		user.setLastName("the Builder");
		user.setCprNumber("mumbai");
		
		
		
		
		try {
			
			//pay.deleteAccount("123456-7890");
			
			System.out.println("createAccountWithBalance(Bob, 100): ");
			id = bank.getBankServicePort().createAccountWithBalance(user,new BigDecimal("100"));
			System.out.println(id);
			System.out.println();
			
			user.setFirstName("Barbie");
			user.setLastName("Girl");
			user.setCprNumber("101010-2020");
			
			System.out.println("createAccountWithBalance(Barbie, 100): ");
			id2 = bank.getBankServicePort().createAccountWithBalance(user,new BigDecimal("100"));
			System.out.println("Returned id: "+id2);
			System.out.println();
			
			System.out.println("transferMoneyFromTo(Barbie to Bob): ");
			//pay.transaction("123456-7890", "101010-2020", new BigDecimal("100"), "Hello world!");
			//bank.getBankServicePort().transferMoneyFromTo(id2, id, new BigDecimal("100"), "For mowing the lawn.");
			System.out.println("This function returns either nothing or an error.");
			System.out.println();
		}catch(BankServiceException_Exception e) {
			System.out.println(e);
			System.out.println();
			
			System.out.println("getAccountByCprNumber(Bob): ");
			Account acc = bank.getBankServicePort().getAccountByCprNumber("mumbai");
			System.out.println("User: "+acc.getUser().getFirstName()+" "+acc.getUser().getLastName());
			System.out.println("Balance: "+acc.getBalance());
			id = acc.getId();
			System.out.println("ID: "+acc.getId());
			System.out.println();
					
			System.out.println("getAccount(Barbie): ");
			acc = bank.getBankServicePort().getAccountByCprNumber("101010-2020");
			System.out.println("User: "+acc.getUser().getFirstName()+" "+acc.getUser().getLastName());
			System.out.println("Balance: "+acc.getBalance());
			id2 = acc.getId();
			System.out.println("ID: "+acc.getId());
			System.out.println();
			
			System.out.println("getAccounts: ");
			List<AccountInfo> result = bank.getBankServicePort().getAccounts();
			System.out.println("Printed list of all accounts: "+result);
			int i = 0;
			for(AccountInfo inf : result)
			{
				i++;
				System.out.println("User "+i+" in list: "+inf.getUser().getFirstName()+" "+inf.getUser().getLastName());
			}
			System.out.println();
			
			System.out.println("retireAccount(Bob): ");
			bank.getBankServicePort().retireAccount(id);
			System.out.println("This function returns either nothing or an error.");
			System.out.println();
			
			System.out.println("retireAccount(Barbie): ");
			bank.getBankServicePort().retireAccount(id2);
			System.out.println("This function returns either nothing or an error.");
			System.out.println();
		}
		
		
		
		
		
	}
}
