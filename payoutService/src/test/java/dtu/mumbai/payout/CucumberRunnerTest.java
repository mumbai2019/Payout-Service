package dtu.mumbai.payout;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/features",
				snippets=SnippetType.CAMELCASE)
public class CucumberRunnerTest {
}