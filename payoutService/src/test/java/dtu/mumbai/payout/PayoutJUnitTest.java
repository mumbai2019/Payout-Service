package dtu.mumbai.payout;
import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dtu.mumbai.payout.BankCommunicationImplementation;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;

/**
 * A set of JUnit tests to test and check code coverage of PayoutImplementation.java
 * @author s144034
 */

public class PayoutJUnitTest {
	BankCommunicationImplementation pay = new BankCommunicationImplementation();
	static BankServiceService bank;
	
	
	@Before
	public void Initialize() throws BankServiceException_Exception {
		pay = new BankCommunicationImplementation();
		bank = new BankServiceService();
		try {
			pay.createAccount("Bob", "The Builder", "418994-1234", new BigDecimal(100));
		}
		catch (BankServiceException_Exception e){
			bank.getBankServicePort().retireAccount(bank.getBankServicePort().getAccountByCprNumber("418994-1234").getId());
			pay.createAccount("Bob", "The Builder", "418994-1234", new BigDecimal(100));			
		}
	
		try {
			pay.createAccount("Starvin", "Marvin", "418994-4567", new BigDecimal(100));
		}
		catch (BankServiceException_Exception e){
			bank.getBankServicePort().retireAccount(bank.getBankServicePort().getAccountByCprNumber("418994-4567").getId());
			pay.createAccount("Starvin", "Marvin", "418994-4567", new BigDecimal(100));		
		}

	}


	@Test (expected = BankServiceException_Exception.class)
	public void CreateAccountThatExist() throws BankServiceException_Exception {
		pay.createAccount("Bob", "The Builder", "418994-1234", new BigDecimal(100));
	}
	
	@Test
	public void GetAccountTest() throws BankServiceException_Exception {		
			Account account = pay.getAccountByCprNumber("418994-1234");
			boolean checkAccount = account.getBalance().equals(new BigDecimal(100)) && account.getUser().getFirstName().equals("Bob") && account.getUser().getLastName().equals("The Builder");
			
			assertTrue(checkAccount);
	}

	@Test
	public void TransactionTest() throws BankServiceException_Exception {
		pay.transaction("418994-1234", "418994-4567", new BigDecimal(20), "Good Riddance");
		Account reciever = pay.getAccountByCprNumber("418994-4567");
		assertEquals(reciever.getBalance(), new BigDecimal(120));
	}
	
	@After
	public void CleanUp() throws BankServiceException_Exception {
		pay.deleteAccount("418994-1234");
		pay.deleteAccount("418994-4567");
	}
	
}
