package enums;

import java.util.Optional;

import org.json.JSONException;
import org.json.JSONObject;

import dtu.mumbai.app.PayoutImplementation;
import dtu.ws.fastmoney.BankServiceException_Exception;

public enum EventEnums 
{
	/**
	 * Used when transmitting messages only.
	 */
	NEWTRANSACTION{
		@Override
		public void handleEvent(PayoutImplementation payout, JSONObject message) {}
		
	},
	/**
	 * Used when transmitting messages only.
	 */
    TOKENCPREVENT
    {
    	@Override
    	public void handleEvent(PayoutImplementation payout, JSONObject message) {}
    },
    /**
     * Handling a cpr authentication event received from token service.
     * Based on whether the cpr passed or failed the authentication, it passes an optional string to payout.
     * A rest interface will be waiting for this optional string to arrive in payout.  
     * @author s160902
     */
    TOKENAUTHCPREVENT{
    	@Override
    	public void handleEvent(PayoutImplementation payout, JSONObject message) {
    		Optional<String> opt = Optional.empty();
    		
    		if(message.getBoolean("result")) {
    			opt = Optional.of(message.getString("cpr"));
    		}
    		
    		payout.setTokenResponse(opt);
		}
    },
    /**
     * Used when transmitting messages only.
     */
    USERVERIFYEVENT{
		@Override
		public void handleEvent(PayoutImplementation payout, JSONObject message) {}
    	
    },
    /**
     * Handling a user authentication event received from token service.
     * Based on whether the cpr passed or failed the authentication, it passes a 1 (true) or 2 (false) to payout.
     * A rest interface will be waiting for this information to arrive in payout.  
     * @author s160902
     */
    USERAUTHEVENT{
    	@Override
    	public void handleEvent(PayoutImplementation payout, JSONObject message) {
    		if(message.getBoolean("response")) {
    			payout.setUserResponse(1);
    		}else {
    			payout.setUserResponse(2);
    		}
		}
    },
    /**
     * Used when transmitting messages only.
     */
    REFUNDVERIFYEVENT{

		@Override
		public void handleEvent(PayoutImplementation payout, JSONObject message) {}
    	
    },
    /**
     * Handling a refund authentication event received from management service.
     * Simply passes on the JSON containing the additional needed information for making a refund.
     * A rest interface will be waiting for this information to arrive in payout.  
     * @author s160902
     */
    REFUNDAUTHEVENT{
		@Override
		public void handleEvent(PayoutImplementation payout, JSONObject message) {
			payout.setRefundResponse(message);
		}
    },
    /**
     * Handling a refund authentication event received from management service.
     * Simply passes on the JSON containing the additional needed information for making a refund.
     * A rest interface will be waiting for this information to arrive in payout.  
     * @author s160902
     */
    REFUNDDONE{
		@Override
		public void handleEvent(PayoutImplementation payout, JSONObject message) {}
    },
    /**
	 * Was to be used if the management service wanted an account created, but was never actually used 
	 * @author s160902
	 */
    CREATEACCOUNT{
		@Override
		public void handleEvent(PayoutImplementation payout, JSONObject message) {
			try {
				payout.getBank().createAccount(
						message.getString("firstname"),
						message.getString("lastname"),
						message.getString("cpr"),
						message.getBigDecimal("amount"));
			} catch (JSONException | BankServiceException_Exception e) {
				System.out.print("BANK EXCEPTION WHEN CREATING ACCOUNT. THIS NEED TO BE REFACTORED");
			}
		}
    },
    /**
	 * Was to be used if the management service wanted an account deleted, but was never actually used 
	 * @author s160902
	 */
    DELETEACCOUNT{
		@Override
		public void handleEvent(PayoutImplementation payout, JSONObject message) {
			try {
				payout.getBank().deleteAccount(message.getString("cpr"));
			} catch (JSONException | BankServiceException_Exception e) {
				System.out.print("BANK EXCEPTION WHEN DELETING ACCOUNT. THIS NEED TO BE REFACTORED");
			}
		}
    },
    /**
     * Used in testing the RabbitMQ communication.
     * Re-uses the setUserResponse function for this test.
     * A rest interface will be waiting for this information to arrive in payout.  
     * @author s160902
     */
    TESTEVENT{
		public void handleEvent(PayoutImplementation payout, JSONObject message) {
			payout.setUserResponse(1);
		}
    	
    };
	
	public abstract void handleEvent(PayoutImplementation payout, JSONObject message);
}