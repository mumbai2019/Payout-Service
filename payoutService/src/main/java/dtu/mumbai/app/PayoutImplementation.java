package dtu.mumbai.app;

import java.util.Optional;

import org.json.JSONObject;

import dtu.mumbai.payout.BankCommunicationImplementation;
import dtu.mumbai.payout.ConsumerImplementation;
import dtu.mumbai.rabbitmq.Receiver;
import dtu.mumbai.rabbitmq.Transmitter;

/**
 * Contains the receiver, the bankCommunication and the transmitter classes, and enables internal communication between them and the rest interface.
 * @author s160902
 */
public class PayoutImplementation {
	private Optional<String> tokenResponse = null;
	private int userResponse = 0;
	private JSONObject refundInformation = null;
	
	private BankCommunicationImplementation bank = new BankCommunicationImplementation();
	private Transmitter transmitter = new Transmitter();
	private ConsumerImplementation consume = new ConsumerImplementation(this);
	
	/**
	 * Creates and runs the receiver.
	 * @author s160902
	 */
	public void runReceiver() {
		Receiver receiver = new Receiver(consume);
		receiver.run();
	}
	/**
	 * Sets the tokenResponse field and notifies any thread potentially waiting for this information.
	 * @param tokenResponse
	 * @author s160902
	 */
	public synchronized void setTokenResponse(Optional<String> tokenResponse) {
		this.tokenResponse = tokenResponse;
		notify();
	}
	/**
	 * Retrieves the response from token service, if it hasn't been received yet, then wait for its arrival.
	 * @author s160902
	 * @return Optional<String>
	 */
	public synchronized Optional<String> getTokenResponse() {
		while(tokenResponse == null)
		{
			try {
				wait();
			} catch (InterruptedException e) {}
		}
		Optional<String> response = tokenResponse;
		tokenResponse = null;
		return response;
	}
	/**
	 * Sets the userResponse field and notifies any thread potentially waiting for this information.
	 * @author s160902
	 * @param tokenResponse
	 */
	public synchronized void setUserResponse(int userResponse) {
		this.userResponse = userResponse;
		notify();
	}
	/**
	 * Retrieves the response from management service, if it hasn't been received yet, then wait for its arrival.
	 * @author s160902
	 * @return boolean
	 */
	public synchronized boolean getUserResponse() {
		while(userResponse==0)
		{
			try {
				wait();
			} catch (InterruptedException e) {}
		}
		boolean response;
		if(userResponse == 1) {
			response = true;
		}else {
			response = false;
		}
		userResponse = 0;
		return response;
	}
	/**
	 * Sets the refund fields and notifies 
	 * @author s160902
	 * @param refundAmount
	 * @param refundCustomer
	 */
	public synchronized void setRefundResponse(JSONObject refundInformation) {
		this.refundInformation = refundInformation;
		notify();
	}
	/**
	 * Retrieves the response from management service, if it hasn't been received yet, then wait for its arrival.
	 * @author s160902
	 * @return response
	 */
	public synchronized JSONObject getRefundResponse() {
		while(refundInformation == null) {
			try {
				wait();
			} catch (InterruptedException e) {}
		}
		JSONObject response = refundInformation;
		refundInformation = null;
		return response;
	}
	/**
	 * @author s160902
	 * @return
	 */
	public Transmitter getTransmitter() {
		return transmitter;
	}
	/**
	 * @author s160902
	 * @return
	 */
	public BankCommunicationImplementation getBank() {
		return bank;
	}
	
}
