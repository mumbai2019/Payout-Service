package dtu.mumbai.rest;

import dtu.mumbai.app.PayoutImplementation;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONException;
import org.json.JSONObject;
import dtu.ws.fastmoney.BankServiceException_Exception;
import enums.EventEnums;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeoutException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;

/**
 * Rest Adapter at the Payout Microservice
 * @author s172596 Diego
 */
@Path("/payout")
public class PayoutEndpoint {
	PayoutImplementation payout = RestApplication.payout;
	Date date;
	SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ss");
	/**
	 * For testing purposes only!
	 * Tests that REST can is up and responding to simple messages.
	 */
	  @GET
	  @Produces("text/plain")
	  public Response doGet() {
	    return Response.ok("Welcome to the payout rest-interface").build();
	  }
	  /**
	   * For testing purposes only!
	   * Tests that the rest interface can receive and return JSON.
	   * @author s160902
	   */
	  @POST
	  @Path("/getTest")
	  @Consumes("application/json")
	  @Produces("application/json")
	  public Response getTest(String inputJsonObj) {
		  JSONObject json = new JSONObject(inputJsonObj);
		  JSONObject json2 = new JSONObject();
		  json2.put("test", json.get("test"));
		  
		  return Response
				  .status(Response.Status.OK)
				  .entity(json2.toString())
				  .type(MediaType.APPLICATION_JSON)
				  .build();
	  }
	  /**
	   * Creates an account in the bank.
	   * @author s160902
	   */
	  @POST
	  @Path("/create")
	  @Consumes("application/json")
	  public Response createAccount(String inputJsonObj) {
		  JSONObject json = new JSONObject(inputJsonObj);
		  
		  try {
			  payout.getBank().createAccount(
					  json.getString("firstName"),
					  json.getString("lastName"),
					  json.getString("cpr"),
					  json.getBigDecimal("initialBalance"));
			  
			  return Response
					  .status(Response.Status.OK)
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		  } catch (Exception e) {
			  return Response
					  .status(Response.Status.CONFLICT)
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		  }
	  }
	  /**
	   * Deletes an account in the bank.
	   * @author s160902
	   */
	  @POST
	  @Path("/delete")
	  @Consumes("application/json")
	  public Response deleteAccount(String input) {
		  try {
			payout.getBank().deleteAccount(new JSONObject(input).getString("cpr"));
			return Response
					  .status(Response.Status.OK)
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		  } catch (Exception e) {
			  return Response
					  .status(Response.Status.CONFLICT)
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		  }
	  }
	  /**
	   * For testing purposes only!
	   * Retrieves the balance on a specified account, so that the balance can be verified.
	   * Used to check that a transaction has made the account balance change.
	   * @author s160902
	   */
	  @POST
	  @Path("/getBalance")
	  @Consumes("application/json")
	  @Produces("application/json")
	  public Response getBalance(String inputJsonObj) {		  
		  try {
			  JSONObject json = new JSONObject(inputJsonObj);
			  BigDecimal balance = payout.getBank().getAccountByCprNumber(json.getString("cpr")).getBalance();
			  
			  JSONObject json2 = new JSONObject();
			  json2.put("balance", balance);
			  
			  return Response
					  .status(Response.Status.OK)
					  .entity(json2.toString())
					  .type(MediaType.APPLICATION_JSON)
					  .build();
			
		} catch (JSONException e) {
			return Response
					  .status(Response.Status.BAD_REQUEST)
					  .entity("There was JSONException: "+e.getMessage())
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		} catch (BankServiceException_Exception e) {
			return Response
					  .status(Response.Status.BAD_REQUEST)
					  .entity("There was BankServiceException: ")
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		}
	  }
	  /**
	   * For testing purposes only!
	   * Tests the RabbitMQ connection between the payout and token service.
	   * @author s160902
	   */
	  @GET
	  @Path("/connect")
	  @Produces("text/plain")
	  public Response connect() {
		  
		  JSONObject json = new JSONObject();
		  json.put("event", EventEnums.TESTEVENT);
		  
		  try {
			  payout.getTransmitter().pushMessageTo(json.toString(), "token");
		  } catch (IOException | TimeoutException e) {
		  }
		  
		  if(RestApplication.payout.getUserResponse()) {}
		  return Response.ok("{GODKENDT!}").build();
	  }
	  /**
	   * Refunds a payment.
	   * Requires an already used token and a merchant CPR that matches the one the token was used with.
	   * @author s160902
	   */
	  @POST
	  @Path("/refund")
	  @Consumes("application/json")
	  @Produces("application/json")
	  public Response refund( String inputJsonObj ) {
		  try {
			  JSONObject jsonInput = new JSONObject(inputJsonObj);
			  
			  // Save merchant for later
			  String merchant = jsonInput.getString("merchant");
			  String token = jsonInput.getString("token");
			  
			  // Prepare message to be handled by the management service
			  jsonInput.put("event", EventEnums.REFUNDVERIFYEVENT);
			  
			  // Push refund message to management service
			  payout.getTransmitter().pushMessageTo(jsonInput.toString(), "manager");
			  
			  // Get response from management service
			  JSONObject jsonOutput = payout.getRefundResponse();
			  
			  // Make refund
			  if(jsonOutput.getBoolean("response")) {
				  String reason = "This refund was brought to you by DTU PAY MUMBAI.";
				  payout.getBank().transaction(merchant, jsonOutput.getString("customer"), jsonOutput.getBigDecimal("amount"), reason);
				  
				  jsonInput = new JSONObject();
				  jsonInput.put("event", EventEnums.REFUNDDONE);
				  jsonInput.put("token", token);
				  //jsonInput.put("date", "");
				  payout.getTransmitter().pushMessageTo(jsonInput.toString(), "manager");
				  
				  return Response
						  .status(Response.Status.OK)
						  .type(MediaType.APPLICATION_JSON)
						  .build();
			  }else {
				  JSONObject json = new JSONObject();
				  json.put("error", "Refund denied");
				  return Response
						  .status(Response.Status.CONFLICT)
						  .entity(json.toString())
						  .type(MediaType.APPLICATION_JSON)
						  .build();
			  }
		  }catch (BankServiceException_Exception e) {
			  JSONObject json = new JSONObject();
			  json.put("error", "Bank Exception: \n" + e);
			  return Response
					  .status(Response.Status.NOT_ACCEPTABLE)
					  .entity(json.toString())
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		  }catch (Exception e) {
			  JSONObject json = new JSONObject();
			  json.put("error",e);
			  return Response
					  .status(Response.Status.NOT_ACCEPTABLE)
					  .entity(json.toString())
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		  }
	  }
	  /**
	   * Handles a REST request for making an transaction.
	   * First it will check the received token is valid at token service, where it will retrieve a cpr number.
	   * Then it will check that the merchant is a user that exists in the management service.
	   * Then it will attempt to make the transaction with the bank.
	   * All types of errors returns an error code, in JSON format, with the key "error", to the client.
	   * Returns OK if the transaction was successful.
	   * Needs other micro-services to respond, or it will forever be stuck, as no time-out has been implemented.
	   * @author s160902 
	   */
	  @POST
	  @Path("/transaction")
	  @Consumes("application/json")
	  @Produces("application/json")
	  public Response transaction( String inputJsonObj ) {
		  try {
			  JSONObject jsonInput = new JSONObject(inputJsonObj);
			  
			  String merchant = jsonInput.getString("merchant");
			  long amount = jsonInput.getLong("amount");
			  String token = jsonInput.getString("token");
			  
			  // Create token message.
			  JSONObject json = new JSONObject();
			  json.put("event", EventEnums.TOKENCPREVENT);
			  json.put("token", token);
			  
			  payout.getTransmitter().pushMessageTo(json.toString(), "token");
			  
			  // Wait for response from token service.
			  Optional<String> response = payout.getTokenResponse();
			  
			  // If token is valid
			  if(response.isPresent()) {
				  
				  // The actual response value
				  String customer = response.get();
				  
				  // Create message for management service.
				  json = new JSONObject();
				  json.put("event", EventEnums.USERVERIFYEVENT);
				  json.put("cpr", merchant);
				  
				  // Push message to management service.
				  payout.getTransmitter().pushMessageTo(json.toString(), "manager");
				  
				  // Wait for a response from management service.
				  if(payout.getUserResponse()) {
					  String reasonForTransfer = "This transaction was brought to you by DTU PAY MUMBAI.";
					  payout.getBank().transaction(customer, merchant, new BigDecimal(amount), reasonForTransfer);
					  
					  // Make and send report to report service.
					  json = new JSONObject();
					  json.put("event", EventEnums.NEWTRANSACTION);
					  json.put("token", token);
					  json.put("merchantCpr", merchant);
					  json.put("customerCpr", customer);
					  json.put("amount", new BigDecimal(amount));
					  json.put("reason", reasonForTransfer);
					  json.put("date", ft.format(new Date())+"");
					  payout.getTransmitter().pushMessageTo(json.toString(), "manager");
					  
					  return Response
							  .status(Response.Status.OK)
							  .type(MediaType.APPLICATION_JSON)
							  .build();
				  }else {
					  json = new JSONObject();
					  json.put("error", "Merchant not registered.");
					  return Response
							  .status(Response.Status.CONFLICT)
							  .entity(json.toString())
							  .type(MediaType.APPLICATION_JSON)
							  .build();
				  }				  
			  }else {
				  json = new JSONObject();
				  json.put("error", "Invalid Token.");
				  return Response
						  .status(Response.Status.CONFLICT)
						  .entity(json.toString())
						  .type(MediaType.APPLICATION_JSON)
						  .build();
			  }
		  } 
		  catch (BankServiceException_Exception e) {
			  JSONObject json = new JSONObject();
			  json.put("error", "Bank Exception: \n"+e.getFaultInfo());
			  return Response
					  .status(Response.Status.CONFLICT)
					  .entity(json.toString())
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		  }	
		  catch (Exception e) {
			  JSONObject json = new JSONObject();
			  json.put("error", "Bank Exception: \n" + e);
			  return Response
					  .status(Response.Status.NOT_ACCEPTABLE)
					  .entity(json.toString())
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		  }	
	  }
}