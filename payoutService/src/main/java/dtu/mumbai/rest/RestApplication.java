package dtu.mumbai.rest;

import javax.ws.rs.core.Application;

import dtu.mumbai.app.PayoutImplementation;

import javax.ws.rs.ApplicationPath;

/**
 * @author s172596 Diego
 */
@ApplicationPath("/") //Don't touch this
public class RestApplication extends Application {
	public static PayoutImplementation payout = new PayoutImplementation();
	public RestApplication(){
		payout.runReceiver();
	}
}