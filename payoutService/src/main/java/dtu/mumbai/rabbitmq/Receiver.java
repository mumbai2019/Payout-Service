package dtu.mumbai.rabbitmq;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import dtu.mumbai.payout.ConsumerImplementation;

import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import com.rabbitmq.client.Channel;


/**
 * Receives messages from RabbitMQ and handles them asynchronously.
 * @author s160902
 */
public class Receiver extends Thread {
	private final static String EXCHANGE_NAME = "serviceExchange";
	private ConsumerImplementation consume;
	
	/**
	 * Receiver needs a consumer.
	 * @author s160902
	 * @param consume
	 */
	public Receiver(ConsumerImplementation consume) {
		this.consume = consume;
	}
	/**
	 * Creates the connection to RabbitMQ and listens for the routing key "payout".
	 * Whenever it receives such message, it executes consumeMessage in Consumer with the message.
	 * @author s160902
	 */
	public void run() {
		try {
			ConnectionFactory factory = new ConnectionFactory();
			//factory.setHost("localhost");
			factory.setHost("rabbitmq");
			
			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();
			
			channel.exchangeDeclare(EXCHANGE_NAME, "direct");
			String queueName = channel.queueDeclare().getQueue();
			
			channel.queueBind(queueName, EXCHANGE_NAME, "payout");
			
			System.out.println("GODDAG PAYOUT RECEIVER.");
			
			DeliverCallback deliverCallback = (consumerTag, delivery) -> {
		        consume.consumeMessage(new String(delivery.getBody(), "UTF-8"));
		    };
			
		    channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
		    
		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
		}
	}
}
