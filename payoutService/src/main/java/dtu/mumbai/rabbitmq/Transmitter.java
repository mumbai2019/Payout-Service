package dtu.mumbai.rabbitmq;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @author s160902
 * Connects to and transmits messages to the RabbitMQ queue.
 */
public class Transmitter {
	private String EXCHANGE_NAME = "serviceExchange";
	
	private Connection connection;
	private Channel channel;
	private boolean firstRun = true;
	/**
	 * @author s160902
	 * @throws IOException
	 * @throws TimeoutException
	 */
	private void startTransmitter() throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
	    //factory.setHost("localhost");
		factory.setHost("rabbitmq");
	    
	    connection = factory.newConnection();
	    channel = connection.createChannel();
	    
	    channel.exchangeDeclare(EXCHANGE_NAME, "direct");
	}
	/**
	 * Sends message to the RabbitMQ exchange called "serviceExchange", to be picked up by the
	 * consumer listening for the specified routingKey.
	 * @author s160902
	 * @param message
	 * @param routingKey
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws TimeoutException 
	 */
	public void pushMessageTo(String message, String routingKey) throws UnsupportedEncodingException, IOException, TimeoutException {
		if(firstRun) {
			firstRun = false;
			startTransmitter();
		}
		
		channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes("UTF-8"));
		System.out.println(" [x] Sent '" + routingKey + "':'" + message + "'");
	}
	
}
