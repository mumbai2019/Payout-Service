package dtu.mumbai.payout;

/**
 * Consumer handles the messages from the receiver.
 * @author s160902
 *
 */
public interface Consumer {
	/**
	 * Handles the message given to it.
	 * @author s160902
	 * @param String message
	 */
	public void consumeMessage(String message);
}
