package dtu.mumbai.payout;

import org.json.JSONObject;

import dtu.mumbai.app.PayoutImplementation;
import enums.EventEnums;

/**
 * ConsumerImplementation handles the messages from the receiver.
 * @author s160902
 *
 */
public class ConsumerImplementation implements Consumer {
	BankCommunicationImplementation bank = new BankCommunicationImplementation();
	PayoutImplementation payout;
	/**
	 * @author s160902
	 * @param payout
	 */
	public ConsumerImplementation(PayoutImplementation payout) {
		this.payout = payout;
		
	}
	/**
	 * Handles the message given to it, based on the enum it contains.
	 * The message must be in JSON format and must contain an EventEnum at the key "event".
	 * @author s160902
	 * @param String message
	 */
	public void consumeMessage(String message) {
		System.out.println("Payout Consumer recieved message: " + message);
		
		JSONObject jsonInput = new JSONObject(message);
		
		EventEnums enumEvent = EventEnums.valueOf(jsonInput.getString("event"));
		enumEvent.handleEvent(payout, jsonInput);
	}
}
