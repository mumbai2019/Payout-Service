package dtu.mumbai.payout;

import java.math.BigDecimal;

import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;

/**
 * A set of functions to communicate with the bank, using client stubs generated using
 * wsimport on the WSDL file found at: "http://02267-kolkata.compute.dtu.dk:8080/BankService?wsdl".
 * @author s160902
 */
public class BankCommunicationImplementation implements BankCommunication
{
	BankServiceService bank = new BankServiceService();
	
	/**
	 * Sends a transaction request to the bank.
	 * @author s160902
	 * @param cprNumberFrom
	 * @param cprNumberTo
	 * @param moneyAmount
	 * @param transactionReason
	 * @throws BankServiceException_Exception
	 */
	public void transaction(String cprNumberFrom, String cprNumberTo, BigDecimal moneyAmount, String transactionReason)
			throws BankServiceException_Exception
	{
		bank.getBankServicePort().transferMoneyFromTo(
				bank.getBankServicePort().getAccountByCprNumber(cprNumberFrom).getId(),
				bank.getBankServicePort().getAccountByCprNumber(cprNumberTo).getId(),
				moneyAmount, transactionReason);
	}
	/**
	 * Sends a account creation request to the bank.
	 * @author s160902
	 * @param firstName
	 * @param lastName
	 * @param cprNumber
	 * @param initialBalance
	 * @throws BankServiceException_Exception
	 */
	public void createAccount(String firstName, String lastName, String cprNumber, BigDecimal initialBalance)
			throws BankServiceException_Exception
	{
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setCprNumber(cprNumber);
		bank.getBankServicePort().createAccountWithBalance(user,initialBalance);
	}
	/**
	 * Sends a delete account request to the bank.
	 * @author s160902
	 * @param cprNumber
	 * @throws BankServiceException_Exception
	 */
	public void deleteAccount(String cprNumber) throws BankServiceException_Exception {
		bank.getBankServicePort().retireAccount(bank.getBankServicePort().getAccountByCprNumber(cprNumber).getId());
	}
	/**
	 * Sends a request to the bank to return the account information of the account identified by the given CPR-number.
	 * @author s160902
	 * @param cprNumber
	 * @return Account
	 * @throws BankServiceException_Exception
	 */
	public Account getAccountByCprNumber(String cprNumber) throws BankServiceException_Exception {
		return bank.getBankServicePort().getAccountByCprNumber(cprNumber);
	}
	/**
	 * Deletes an account if it exists.
	 * For testing purposes ONLY!
	 * @author s160902
	 * @param cprNumber
	 * @throws BankServiceException_Exception
	 */
	public void testCleanup(String cprNumber) {
		try {
			bank.getBankServicePort().retireAccount(bank.getBankServicePort().getAccountByCprNumber(cprNumber).getId());
		}catch(BankServiceException_Exception e) {
			
		}
	}
}
