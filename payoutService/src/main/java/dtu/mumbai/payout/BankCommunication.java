package dtu.mumbai.payout;

import java.math.BigDecimal;

import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankServiceException_Exception;

/**
 * Interface used in PayoutImplementation.
 * @author s160902
 */
public interface BankCommunication
{
	/**
	 * @author s160902
	 * Sends a request to the bank to make a transaction between two accounts, identified by cpr-number.
	 * @param cprNumberFrom
	 * @param cprNumberTo
	 * @param moneyAmount
	 * @param transactionReason
	 * @throws BankServiceException_Exception
	 */
	public void transaction(String cprNumberFrom, String cprNumberTo, BigDecimal moneyAmount, String transactionReason) throws BankServiceException_Exception;
	/**
	 * @author s160902
	 * Sends a request to the bank to create an account with the given values.
	 * @param firstName
	 * @param lastName
	 * @param cprNumber
	 * @param initialBalance
	 * @throws BankServiceException_Exception
	 */
	public void createAccount(String firstName, String lastName, String cprNumber, BigDecimal initialBalance) throws BankServiceException_Exception;
	/**
	 * @author s160902
	 * Sends a request to the bank to delete the account identified by the given cpr-number.
	 * @param cpr
	 * @throws BankServiceException_Exception
	 */
	public void deleteAccount(String cprNumber) throws BankServiceException_Exception;
	/**
	 * @author s160902
	 * Sends a request to the bank to return the account information of the account identified by the given CPR-number.
	 * @param cprNumber
	 * @return Account
	 * @throws BankServiceException_Exception
	 */
	public Account getAccountByCprNumber(String cprNumber) throws BankServiceException_Exception;
	/**
	 * @author s160902
	 * Deletes an account if it exists.
	 * For testing purposes ONLY!
	 * @param cprNumber
	 * @throws BankServiceException_Exception
	 */
	public void testCleanup(String cprNumber);
	
}