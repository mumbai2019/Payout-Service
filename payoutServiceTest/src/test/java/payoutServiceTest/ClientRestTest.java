package payoutServiceTest;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import mumbai.common.restClient.RestClient;

/**
 * JUnit tests for the REST adapter in the Payout Microservice
 * @author s172596 Diego
 *
 */
public class ClientRestTest {

	RestClient r;
	@Before
	public void init() {
	r =  new RestClient("http://02267-mumbai.compute.dtu.dk:4000");
	
	JSONObject jObject = new JSONObject();
	jObject.put("cpr", "mumbaiTestOne");
	
	}
	
	@Test
	@Ignore
	public void testTransaction() {
		
		JSONObject jObject = new JSONObject();
		jObject.put("token", 2);
		jObject.put("merchant", "mumbaiTestOne");
		jObject.put("amount", 100);
		
		System.out.println("JSON HERE: "+jObject.toString());
		Response web = r.post("payout/transaction", jObject.toString(), MediaType.APPLICATION_JSON);
		if(web.getStatus() == 200)
			System.out.println("TRANSACTION GOT AN OK");
		if( web.getStatus() == 400 )
			System.out.println("TRANSACTION ERROR HERE: " + web.readEntity(String.class));
		System.out.println("RESULT: " + web.readEntity(String.class));		
		assertEquals( 200, web.getStatus());
		
	}	

}
